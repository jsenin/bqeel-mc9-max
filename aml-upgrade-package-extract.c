/*
 * extracted from https://www.cnx-software.com/2016/11/19/how-to-create-a-bootable-recovery-sd-card-for-amlogic-tv-boxes/
 * extracted from http://freaktab.com/forum/tv-player-support/amlogic-based-tv-players/s912/others-aml-s912/firmware-roms-tools-by/602802-aml_upgrade_package-img-extract
 *
 * Build:
 * gcc aml-upgrade-package-extract.c -o aml-upgrade-package-extract
 *
 * Execute:
 * ./aml-upgrade-package-extract aml_s912_q6330-R-BOX-PRO-3gddr-mac-20161015.img
 *
 * Prepare bootable sdcard
 *
 * select your sdcard using lsblk to find your devices:
 * lsblk -o kname,size,name,type,tran,mountpoint,model,label | grep disk
 *
 * Ensure your sdcard is an fat32 formated card with one partition
 *
 * Umount device and dump boot partition to the sdcard
 * sudo umount /dev/sdX*
 * sudo dd if=aml_sdc_burn.UBOOT of=/dev/sdX bs=1 count=442
 * sudo dd if=aml_sdc_burn.UBOOT of=/dev/sdX seek=1 skip=1 bs=512
 *
 * mount again the card and copy files
 * cp -v aml_sdc_burn.{ini,UBOOT} [device-mount-point]
 * cp -v update-usb-burning-mode.img [device-mount-point]/aml_upgrade_package.img
 * image file must be aml_upgrade_package.img in order to match with the name configured at aml_sdc_burn.ini 
 *
 * start the upgrade:
 * disconect power cord from your tv box
 * insert card into your tv box
 * press the reset button
 * plug the power cord
 * must appear an screen with the green android robot, a progress bar and a label 'upgrading'
 * when finish an ok tick should appear
 * reboot your device
 * good look
 */


#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
 
uint32_t convert(uint8_t *test, uint64_t loc) {
  return ntohl((test[loc] << 24) | (test[loc+1] << 16) | (test[loc+2] << 8) | test[loc+3]);
}
 
void main (int argc, char **argv) {
  FILE *fileptr;
  uint8_t *buffer;
  long filelen;
 
  FILE *f;
  char *filename;
  uint64_t record;
  uint64_t record_loc;
  uint64_t file_loc;
  uint64_t file_size;
 
  if (argc <= 1) {
    printf("Usage: %s [firmware-file-name]\n", argv[0]);
    exit (0);
  }
 
  fileptr = fopen(argv[1], "rb");
  fseek(fileptr, 0, SEEK_END);
  filelen = ftell(fileptr);
  rewind(fileptr);
 
  buffer = (uint8_t *)malloc((filelen+1)*sizeof(uint8_t));
  fread(buffer, filelen, 1, fileptr);
  fclose(fileptr);
 
  for (record = 0; record < (uint8_t)buffer[0x18]; record = record + 1){
    record_loc = 0x40 + (record * 0x240);
 
    filename = (malloc(32));
    sprintf(filename,"%s.%s",(char *)&buffer[record_loc+0x120], (char *)&buffer[record_loc+0x20]);
 
    file_loc = convert(buffer,record_loc+0x10);
    file_size = convert(buffer,record_loc+0x18);
 
    f = fopen(filename, "wb");
    if (f == NULL) {
     printf("ERROR: could not open output\n");
     printf("the error was: %s\n",strerror(errno));
     free(filename);
     continue;
    }
    fwrite(&(buffer[file_loc]), sizeof(uint8_t), (size_t)file_size, f);
    fclose(f);
    free(filename);
  }
  free(buffer);
}
