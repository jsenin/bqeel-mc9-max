# BQEEL MC9 MAX

## BOARD: p212

## hardware

- soc: p212
- Marca 	Bqeel
- Fabricante 	Bqeel Trade company
- Modelo 	M9C MAX-EU-3.23
- CPU: S905X quad-core cortex-A53 frequency – 2.0G
- GPU: Mali-450 5-Core GPU
- FLASH: 16GB emmc
- SDRAM: 2GB DDR
- Power Supply: DC 5V/2A
- WIFI: RTL8189 802.11b/g/n
- High Difinition video output: SD/HD max.1920×1080 pixel
- LAN Ethernet: 10/100M, standard RJ-45
- Wireless: Built in WiFi

### pictures
![p212 board picture front side](https://gitlab.com/jsenin/bqeel-mc9-max/-/blob/master/photo5791774913919563298.jpg "board picture front side")
![p212 board picture back side](https://gitlab.com/jsenin/bqeel-mc9-max/-/blob/master/photo5791774913919563299.jpg "board picture back side")


## stuff

linux for meson cpus
* http://linux-meson.com/doku.php
* Meson-GXL / S905X
* Amlogic P212 Reference Design (arch/arm64/boot/dts/amlogic/meson-gxl-s905x-p212.dts)
* is it possible to configure U-Boot such that I can load it into RAM instead of flash, and start it from my old boot loader? http://www.denx.de/wiki/view/DULG/CanUBootBeConfiguredSuchThatItCanBeStartedInRAM
* uboot corrupted https://forum.libreelec.tv/thread/13466-u-boot-corrupted-boot-only-via-sd-s905x-8gb-1gb/
* amlogic opensource tools https://forum.xda-developers.com/android-stick--console-computers/amlogic/opensource-amlogic-tools-t3786991
* amlogic opensource tools https://github.com/Ricky310711/OpenSource-AMLogic-Tools
* amlogic tools https://github.com/osmc/aml-flash-tool
* creating custom firmware amlogic https://www.youtube.com/watch?v=FFqZsJLjYbg
* uboot usage https://docs.khadas.com/vim1/UBootUsage.html
* uboot upgrade https://docs.khadas.com/vim1/HowToUpgradeTheUboot.html
* uboot upgrade via cable  https://docs.khadas.com/vim1/UpgradeViaUSBCable.html
* uboot boot into upgrade mode https://docs.khadas.com/vim1/HowtoBootIntoUpgradeMode.html
* uboot upgrade via sd card https://docs.khadas.com/vim1/UpgradeViaTFBurningCard.html
* amlogic android open source project AOSP https://android-review.googlesource.com/q/amlogic
* howto cardmaker https://www.getdroidtips.com/download-amlogic-burn-card-maker/

### recover only uboot using cable
https://github.com/superna9999/aml-usb-load-uboot

## unbrick

https://github.com/150balbes/Amlogic_s905/wiki/s905_Unbrik_Amlogic_S905
extracting bootloader from rom: https://github.com/3F/aml_s905_uboot
obtaining uboot biinaries: https://forum.armbian.com/topic/9768-amlogic-mainline-u-boot-compilation/?do=findComment&comment=73658


from uboot

- run update: Execute update from sdcard
- update: Execute update from usb cable with aml-flash


## uboot 
howto crosscompile 
http://wiki.loverpi.com/faq:sbc:libre-aml-s805x-howto-compile-u-boot




## udev

```
cat /etc/udev/rules.d/70-persistent-usb-amlogic.rules
SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTR{idVendor}=="1b8e", ATTR{idProduct}=="c003", MODE:="0666", SYMLINK+="worldcup"

sudo udevadm trigger
```


## aml flash
reference: http://openlinux.amlogic.com:8000/download/A113/DOC/Amlogic_update_usb_tool_user_guide.pdf
> WorldCup Device:
> It’s USB device protocol used by update tool to communicate with Amlogic Board, which is in usb burning mode. Like fastboot protocol, update tool can work only when there is a WorldCup device in PC device manager.


```
aml-flash --img=aml_upgrade_package.img --soc=gxl --wipe --reset=n --parts=all
```


## fix from atvXperience
https://forum.atvxperience.com/viewtopic.php?p=14950#p14956

if it doesn't work, you have to partition emmc by hand..
Download this _aml_dtb.PARTITION(https://mega.nz/file/vAhGzCAS#2tZivkvo_Zs0FyGVtLoRZk4GL6MfAoEHZDHx32M7d8c) file and copy it to the mSD card (FAT partition) that has already been prepared with amlogic burn card maker, rename aml_sdc_burn.ini to aml_sdc_burn.ini.bak and then insert the mSD card into the box, execute the following commands:

```
store init 3
mmcinfo; 
# usb
fatload usb 0 ${loadaddr} _aml_dtb.PARTITION 
# sdcard
fatload mmc1 0 ${loadaddr} _aml_dtb.PARTITION
store dtb write ${loadaddr}
```

remove the mSD card from the box and rename aml_sdc_burn.ini.bak back to aml_sdc_burn.ini, reinsert the mSD card into the box and


```
reset
```

## diagnose wifi driver

```
udevadm info /sys/bus/sdio/devices/*

P: /devices/d0070000.sdio/mmc_host/sdio/sdio:0001/sdio:0001:1
L: 0
E: DEVPATH=/devices/d0070000.sdio/mmc_host/sdio/sdio:0001/sdio:0001:1
E: DRIVER=rtl8189fs
E: SDIO_CLASS=07
E: SDIO_ID=024C:F179
E: MODALIAS=sdio:c07v0
```


## list devices at uboot

MMC card / sdcard

```
* No device specified **
gxl_p212_v1#fatls mmc1 0
card in
co-phase 0x2, tx-dly 0, clock 400000
co-phase 0x2, tx-dly 0, clock 400000
co-phase 0x2, tx-dly 0, clock 400000
co-phase 0x2, tx-dly 0, clock 400000
co-phase 0x2, tx-dly 0, clock 40000000
init_part() 282: PART_TYPE_DOS
[mmc_init] mmc init success
            system volume information/
   885248   aml_sdc_burn.uboot
 1570486976   atvx-s905x_rtl_ssv_v4.img
            lost.dir/
            android/
   174080   _aml_dtb.partition
      392   aml_sdc_burn.ini.bak

4 file(s), 3 dir(s)

```

## bricked box
when device it's bricked due a failed upgrade or something that let the box without boot we can see this log at rs232.

with usb expects to execute from aml-flash
```
GXL:BL1:9ac50e:a1974b;FEAT:ADFC318C;POC:3;RCY:0;EMMC:0;READ:0;CHK:AA;SD:800;USB:8;
```
with power supply, starts when detect sdcard
```
GXL:BL1:9ac50e:a1974b;FEAT:ADFC318C;POC:3;RCY:0;EMMC:0;READ:0;CHK:AA;SD:800;USB:8;LOOP:1;EMMC:0;READ:0;CHK:AA;SD:800;USB:8;LOOP:2;EMMC:0;READ:0;CHK:AA;SD:0;READ:0;0.0;CHK:0;

```

with usb plugged-kin the WorldCup driver is up

```
Oct 26 00:40:03 laptop kernel: [115689.279028] usb 3-3: new high-speed USB device number 70 using xhci_hcd
Oct 26 00:40:03 laptop kernel: [115689.428321] usb 3-3: New USB device found, idVendor=1b8e, idProduct=c003, bcdDevice= 0.20
Oct 26 00:40:03 laptop kernel: [115689.428328] usb 3-3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
Oct 26 00:40:03 laptop kernel: [115689.428331] usb 3-3: Product: GX-CHIP
Oct 26 00:40:03 laptop kernel: [115689.428334] usb 3-3: Manufacturer: Amlogic
Oct 26 00:40:03 laptop mtp-probe: checking bus 3, device 70: "/sys/devices/pci0000:00/0000:00:14.0/usb3/3-3"
Oct 26 00:40:03 laptop mtp-probe: bus: 3, device: 70 was not an MTP device
Oct 26 00:40:03 laptop mtp-probe: checking bus 3, device 70: "/sys/devices/pci0000:00/0000:00:14.0/usb3/3-3"
Oct 26 00:40:03 laptop mtp-probe: bus: 3, device: 70 was not an MTP device
```


## kernel debug
```

lrw-r--r--   1 root   root        17 2020-10-17 17:52 d -> /sys/kernel/debug@

console:/d # ls
aml_atvdemod   clk                hid       pm_genpd       suspend_stats
aml_clk        cma                ieee80211 pm_qos         sync
aml_clkmsr     di                 iio       pstore         tracing
aml_reg        dma_buf            ion       pwm            ubi
android_verity emmc               l2tp      regmap         ubifs
asoc           ethclk81           lockup    regulator      usb
bdi            extfrag            mali      sched_features vdec_profile
binder         fault_around_bytes memblock  sd             vdin0
bluetooth      gpio               opp       sdio           video
c9000000.dwc3  hdr                pinctrl   sleep_time     wakeup_sources

console:/d # cat gpio
gpiochip1: GPIOs 401-500, parent: platform/pinctrl@4b0, periphs-banks:
 gpio-422 (                    |mute_gpio           ) out hi
 gpio-436 (                    |amlsd               ) out hi
 gpio-449 (                    |amlsd               ) in  hi
 gpio-474 (                    |sysled              ) out hi
 gpio-486 (                    |sdio_wifi           ) out hi
 gpio-487 (                    |sdio_wifi           ) in  hi
 gpio-497 (                    |bt_rfkill           ) out hi

gpiochip0: GPIOs 501-511, parent: platform/pinctrl@14, aobus-banks:
 gpio-503 (                    |key                 ) in  hi
 gpio-506 (                    |d0078080.usb3phy    ) out hi

console:/d # cat usb/devices

T:  Bus=01 Lev=00 Prnt=00 Port=00 Cnt=00 Dev#=  1 Spd=480  MxCh= 2
B:  Alloc=  0/800 us ( 0%), #Int=  0, #Iso=  0
D:  Ver= 2.00 Cls=09(hub  ) Sub=00 Prot=01 MxPS=64 #Cfgs=  1
P:  Vendor=1d6b ProdID=0002 Rev= 4.09
S:  Manufacturer=Linux 4.9.113 xhci-hcd
S:  Product=xHCI Host Controller
S:  SerialNumber=xhci-hcd.0.auto
C:* #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=  0mA
I:* If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=hub
E:  Ad=81(I) Atr=03(Int.) MxPS=   4 Ivl=256ms

T:  Bus=01 Lev=01 Prnt=01 Port=01 Cnt=01 Dev#=  3 Spd=480  MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=0bda ProdID=f179 Rev= 0.00
S:  Manufacturer=Realtek
S:  Product=802.11n
S:  SerialNumber=002E2D5D3D84
C:* #Ifs= 1 Cfg#= 1 Atr=a0 MxPwr=500mA
I:* If#= 0 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=ff Driver=(none)
E:  Ad=81(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=02(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=03(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms

T:  Bus=02 Lev=00 Prnt=00 Port=00 Cnt=00 Dev#=  1 Spd=5000 MxCh= 0
B:  Alloc=  0/800 us ( 0%), #Int=  0, #Iso=  0
D:  Ver= 3.00 Cls=09(hub  ) Sub=00 Prot=03 MxPS= 9 #Cfgs=  1
P:  Vendor=1d6b ProdID=0003 Rev= 4.09
S:  Manufacturer=Linux 4.9.113 xhci-hcd
S:  Product=xHCI Host Controller
S:  SerialNumber=xhci-hcd.0.auto
C:* #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=  0mA
I:* If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=(none)
E:  Ad=81(I) Atr=03(Int.) MxPS=   4 Ivl=256ms


```
